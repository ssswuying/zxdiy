// pages/manage/coupon-add/index.js
const util = require("../../../utils/util.js");
const db = wx.cloud.database();
const _ = db.command
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id:'',
    yhqname: '',
    starttime: new Date().getTime(),
    endtime: new Date().getTime(),
    starttimeT: util.formatTimeNoSecond(new Date()),
    endtimeT: util.formatTimeNoSecond(new Date()),
    ffsl: 1,
    mrxl: 1,
    sytj: 1,
    yhje: 1,
    state: 1,
    show: {
      middle: false,
      top: false,
      bottom: false,
      bottom2: false,
      right: false,
      right2: false
    },
    minHour: 10,
    maxHour: 20,
    minDate: new Date().getTime(),
    maxDate: new Date(new Date().setDate(new Date().getDate() + 30)).getTime()
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (options.id == null || options.id == '') {
      
    } else {
      this.data.id = options.id;
      db.collection('coupon_info').where({
        _id: _.eq(options.id)
      }).get().then(res => {
        //TODO
        this.setData({
          t:1,
          id: options.id,
          yhqname: res.data[0].yhqname,
          starttime: res.data[0].starttime,
          starttimeT: util.formatTimeNoSecond(res.data[0].starttime),
          endtime: res.data[0].endtime,
          endtimeT: util.formatTimeNoSecond(res.data[0].endtime),
          ffsl: res.data[0].ffsl,
          mrxl: res.data[0].mrxl,
          sytj: res.data[0].sytj,
          yhje: res.data[0].yhje,
          state: res.data[0].state,
        })
      })
    }

  },
  toggleBottomPopup() {
    this.toggle('bottom');
  } ,
  toggleBottomPopup2() {
    this.toggle('bottom2');
  },
  toggle(type) {
    this.setData({
      [`show.${type}`]: !this.data.show[type]
    });
  },
  onChangeName:function(e){
    console.log(e)
    const that = this;
    that.setData({
      yhqname:e.detail
    })
  },
  onConfirmStartTime(event) {
    console.log(event)
    this.setData({
      starttime:event.detail,
      starttimeT: util.formatTimeNoSecond(event.detail)
    })
    this.toggleBottomPopup();
  },
  onConfirmEndTime(event) {
    console.log(event)
    this.setData({
      endtime: event.detail,
      endtimeT: util.formatTimeNoSecond(event.detail)
    })
    this.toggleBottomPopup2();
  },
  onChangeFfsl(e){
    this.setData({
      ffsl:e.detail
    })
  },
  onChangeMrxl(e){
    this.setData({
      mrxl:e.detail
    })
  },
  onChangeSytj(e){
    this.setData({
      sytj:e.detail
    })
  },
  onChangeYhje(e){
    this.setData({
      yhje:e.detail
    })
  },
  onChangeState(e){
    this.setData({
      state:e.detail?1:0
    })
  },
  onClickSave(){
    if (this.data.yhqname == null || this.data.yhqname == '') {
      wx.showToast({
        title: '优惠券名称不能为空',
        icon: 'none',
        duration: 2000
      })
      return;
    }

    wx.showLoading({
      title: '数据同步中',
    })
    wx.cloud.callFunction({
      // 云函数名称
      name: 'coupon-add',
      // 传给云函数的参数
      data: {
        t:0,
        id: this.data.id,
        diyId:wx.getStorageSync('diyId'),
        yhqname: this.data.yhqname,
        starttime: this.data.starttime,
        endtime: this.data.endtime,
        starttimeT: this.data.starttimeT,
        endtimeT: this.data.endtimeT,
        ffsl: this.data.ffsl,
        mrxl: this.data.mrxl,
        sytj: this.data.sytj,
        yhje: this.data.yhje,
        createtime: util.formatTime(new Date()),
        state: this.data.state
      },
      complete: res => {
        this.showSubSuccess();

        console.log('callFunction test result: ', res)
      }
    })
  },
  showSubSuccess: function (e) {
    wx.showToast({
      title: '提交成功',
    })
    setTimeout(function () {
      wx.hideLoading()
      let pages = getCurrentPages();
      let prevPage = pages[pages.length - 2];
      prevPage.setData({
        req: 1
      })
      wx.navigateBack()
    }, 1000)

  }
})