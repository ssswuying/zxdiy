// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: 'product-6d4b5e'
})
const db = cloud.database()
// 云函数入口函数
exports.main = async (event, context) => {
  try {
    if(event.doit == 'qxdd'){  //取消订单
      return await db.collection('order_info').doc(event.id).update({
        // data 传入需要局部更新的数据
        data: {
          // 表示将 done 字段置为 true
          state :-1 //订单关闭
        }
      })
    } else if (event.doit == 'qrsh'){ //确认收货
    console.log(event.id)
      const dd = await db.collection('order_info').doc(event.id).get();
      //日订单统计 日销售额统计
      var newDate = dd.data.createtime.split(" ")[0];
      var newYear = dd.data.createtime.split("-")[0];
      console.log(newDate)
      const pro4 = await db.collection('order_ana').where({ date: newDate,diyId:event.diyId}).get();
      if (pro4.data.length > 0) {
        await db.collection('order_ana').doc(pro4.data[0]._id).update({
          data: {
            // 表示将 done 字段置为 true
            sale: pro4.data[0].sale + dd.data.price
          }
        })
      } else {
        await db.collection('order_ana').add({
          // data 字段表示需新增的 JSON 数据
          data: {
            diyId:event.diyId,
            date: newDate,
            sale: dd.data.price
          }
        })
      }

      const pro5 = await db.collection('order_ana').where({ date: newYear,diyId:event.diyId }).get();
      if (pro5.data.length > 0) {
        await db.collection('order_ana').doc(pro5.data[0]._id).update({
          data: {
            // 表示将 done 字段置为 true
            sale: pro5.data[0].sale + dd.data.price
          }
        })
      } else {
        await db.collection('order_ana').add({
          // data 字段表示需新增的 JSON 数据
          data: {
            diyId:event.diyId,
            date: newYear,
            sale: dd.data.price
          }
        })
      }
      return await db.collection('order_info').doc(event.id).update({
        // data 传入需要局部更新的数据
        data: {
          // 表示将 done 字段置为 true
          state: 3 //订单完成 确认收货
        }
      })
    } else if (event.doit == 'tk') { //退款
      return await db.collection('order_info').doc(event.id).update({
        // data 传入需要局部更新的数据
        data: {
          // 表示将 done 字段置为 true
          check: 1, //退款
          tkyy:event.tkyy,
          tkje:event.tkje,
          tkwtms:event.tkwtms
        }
      })
    } else if (event.doit == 'fh') { //发货
      return await db.collection('order_info').doc(event.id).update({
        // data 传入需要局部更新的数据
        data: {
          // 表示将 done 字段置为 true
          state:2  //订单完成 确认收货
        }
      })
    } else if (event.doit == 'shsq') { //审核申请
      return await db.collection('order_info').doc(event.id).update({
        // data 传入需要局部更新的数据
        data: {
          // 表示将 done 字段置为 true
          check: 1  //审核
        }
      })
    } else if (event.doit == 'shtg') { //审核同意
      return await db.collection('order_info').doc(event.id).update({
        // data 传入需要局部更新的数据
        data: {
          // 表示将 done 字段置为 true
          check: 2  //订单完成 确认收货
        }
      })
    } else if (event.doit == 'shjj') { //拒绝审核
      return await db.collection('order_info').doc(event.id).update({
        // data 传入需要局部更新的数据
        data: {
          // 表示将 done 字段置为 true
          check: 3  //订单完成 确认收货
        }
      })
    } else if (event.doit == 'hh') {
      return await db.collection('order_info').doc(event.id).update({
        // data 传入需要局部更新的数据
        data: {
          // 表示将 done 字段置为 true
          state: 5 //换货
        }
      })
    }

    
   
  } catch (e) {
    console.error(e)
  }
}