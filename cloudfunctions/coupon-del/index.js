const cloud = require('wx-server-sdk')
cloud.init({
  env: 'product-6d4b5e'
})
const db = cloud.database()
exports.main = async (event, context) => {
  try {
    return await db.collection('coupon_info').doc(event.id).remove()
  } catch (e) {
    console.error(e)
  }
}